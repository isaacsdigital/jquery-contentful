# jQuery Contentful

see: [https://www.contentful.com/](https://www.contentful.com/)

## Overview

What is Contentful? A content delivery backend with a RESTful JSON API, plus a web app for managing the content.

The purpose for this plugin is to give HTML developers an easy method
to embed content from Contentful into their web pages.

## Usage

1. Include the javascript in your page
        
        <script type="text/javascript" src="//code.jquery.com/jquery-2.2.2.min.js"></script>
        <script type="text/javascript" src="jquery.contentful.js"></script>

2. Initialize and configure the plugin
        
        <script type="text/javascript">
            $(document).ready(function(){
               $(document).contentful({
                   accessToken: "07b5a78f9dcc2a367fc96d31685a3620902e504b4181ec26021ebc469fb5f10e",
                   spaceId: "74yhgzuqys2o"
               }); 
            });
        </script>

3. Structure your html elements as follows
        
        <div class="contentful-entry" 
            data-id="pBM5Q3SYDua8ugc6Ey4qo" 
            data-field="content" 
            data-parse-markdown="true">
        </div>
        <div class="contentful-entry" data-id="2pWcAFVsO4I0O4M2Yq2mye" data-field="slug"></div>
        <div class="contentful-entry" data-id="2JQA6fr5e0sy2CQAM8e0MU" data-field="slug"></div>
