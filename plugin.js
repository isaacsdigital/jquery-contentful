/**
 * jQuery Contentful
 * 
 * &copy; 2016 EdgeBrands LLC
 * 
 * @author  Fred Isaacs <fred@edgebrandsllc.com>
 * 
 * Config Values:
 * {
 *    accessToken:  string,
 *    spaceId:  string
 * }
 */ 
var showdown = require("showdown");

 
(function($){
    $.fn.contentful = function(config) {

        // make sure the configuration object has all the proper values
        if(!config.hasOwnProperty("accessToken") || !config.hasOwnProperty("spaceId")) {
            console.log("Error.  You must pass an access token and a space id");
            return false;
        }
        
        // markdown converter
        var converter = new showdown.Converter();
        
        
        // base url for all api requests
        var baseUrl = "https://cdn.contentful.com/spaces/" + config.spaceId;
        
        
        // iterate every element with a classname of contentful-entry
        $(".contentful-entry").each(function(index, element){
            // element
            var $el = $(element);
            
            // element's data attributes
            var entryId = $el.data("id");  
            var field = $el.data("field");
            var parseMarkdown = $el.data("parse-markdown");
            
            
            // concatenate values to create full url endpoint to content from api
            var url = baseUrl + "/entries/" + entryId + "?access_token=" + config.accessToken; 
            
            console.log(url, "url endpoint");
            
            // make http request to url endpoint and process the response
             $.getJSON(url,function(response) {
                 console.log(response, "response");
                
                 // if the response has the proper member variables, embed it into the host element
                 if(response.hasOwnProperty("fields") && response.fields.hasOwnProperty(field)){
                    var content = response.fields[field];
                    if(parseMarkdown !== null && parseMarkdown !== false) {
                         content = converter.makeHtml(content);
                    } 
                     
                    $el.html(content);
                 }
            });
        });
    };
})(jQuery)